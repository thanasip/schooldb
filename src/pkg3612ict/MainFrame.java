/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3612ict;

import java.awt.*;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.*;
import javax.swing.event.*;

/*
** PERMISSIONS:
** A - ADMIN
** D - DISTRICT
** S - SCHOOL
** E - DEPARTMENT
** C - CLASS
*/
/**
 *
 * @author Thanasi
 */
public class MainFrame extends javax.swing.JFrame {
    static String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    static String DB_URL = "";
    static String USER = "";
    static String PASS = "";
    static boolean defaultValues = false;
    static boolean uniValues = false;
    Connection conn = null;
    PreparedStatement pstmt = null;
    User logged = new User();
    int tab = 0;
    
    private void getInfo() {
        if (defaultValues) {
            DB_URL = "jdbc:oracle:thin:@//localhost:1521/XE";
            USER = "Thanasi3";
            PASS = "thanos3";
        } else if (uniValues) {
            DB_URL = "jdbc:oracle:thin:@dwarf.cit.griffith.edu.au:1526:DBS";
            USER = "s2703592";
            PASS = "capn1234";
        } else {
            String hostname = "";
            String port = "";
            String sidserv = "";
            boolean service = false;
            JTextField tf = new JTextField();
            JPasswordField pf = new JPasswordField();
            JComboBox cb = new JComboBox<String>(new String[]{"", "SID", "SERVICE"});
            tf.setRequestFocusEnabled(true);

            JOptionPane.showMessageDialog(
                mainpane,
                tf,
                "Enter Hostname",
                JOptionPane.QUESTION_MESSAGE
            );
            if (tf.getText().isEmpty()) System.exit(0);
            hostname = tf.getText();
            tf.setText("");

            JOptionPane.showMessageDialog(
                mainpane,
                tf,
                "Enter Port",
                JOptionPane.QUESTION_MESSAGE
            );
            if (tf.getText().isEmpty()) System.exit(0);
            port = tf.getText();
            tf.setText("");

            JOptionPane.showMessageDialog(
                mainpane,
                cb,
                "Service or SID?",
                JOptionPane.QUESTION_MESSAGE
            );
            if (cb.getSelectedIndex() == 2) {
                service = true;
                JOptionPane.showMessageDialog(
                    mainpane,
                    tf,
                    "Enter Service Name",
                    JOptionPane.QUESTION_MESSAGE
                );
                if (tf.getText().isEmpty()) System.exit(0);
                sidserv = tf.getText();
                tf.setText("");
            } else if (cb.getSelectedIndex() == 1) {
                JOptionPane.showMessageDialog(
                    mainpane,
                    tf,
                    "Enter SID",
                    JOptionPane.QUESTION_MESSAGE
                );
                if (tf.getText().isEmpty()) System.exit(0);
                sidserv = tf.getText();
                tf.setText("");
            } else {
                System.exit(0);
            }

            if (service) {
                DB_URL = ("jdbc:oracle:thin:@//" + hostname + ":" + port + "/" + sidserv);
            } else {
                DB_URL = ("jdbc:oracle:thin:@" + hostname + ":" + port + ":" + sidserv);
            }

            JOptionPane.showMessageDialog(
                mainpane,
                tf,
                "Enter Username",
                JOptionPane.QUESTION_MESSAGE
            );
            if (tf.getText().isEmpty()) System.exit(0);
            USER = tf.getText();
            tf.setText("");

            JOptionPane.showMessageDialog(
                mainpane,
                pf,
                "Enter Password",
                JOptionPane.QUESTION_MESSAGE
            );
            if (String.valueOf(pf.getPassword()).isEmpty()) System.exit(0);
            PASS = String.valueOf(pf.getPassword());
            pf.setText("");
        }
    }
    
    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);  
    }
    /**
     * Creates new form MainFrame
     */
    
    public MainFrame() {
        initComponents();
        int components = mainpane.getComponentCount();
        boolean err = false;
        
        //-----GET DATABASE INFORMATION-----//
        //getInfo();
        //-----GET DATABASE INFORMATION-----//
        
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    conn.close();
                    System.out.println("Connection Closed...");
                    pstmt.close();
                    System.out.println("PreparedStatement Closed...");
                } catch (Exception e) {
                    //Ignore it, we're shutting down anyway
                } finally {
                    System.out.println("Exiting...");
                }
            }
        });
        
        mainpane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                tab = mainpane.getSelectedIndex();
                System.out.println("Tab: " + tab);
                System.out.println("Tab Name: " + mainpane.getTitleAt(tab));
                System.gc();
                
                //-----TAB SWITCHING SQL CODE-----//
                if (mainpane.getTitleAt(tab).equals("Admin")) {
                    ResultSet rs = null;
                    try {
                        addusrschoolcombo.removeAllItems();
                        String sql = "SELECT sname FROM school ORDER BY sname ASC";
                        pstmt = conn.prepareStatement(sql);
                        rs = pstmt.executeQuery();
                        addusrschoolcombo.addItem(new String(""));
                        while (rs.next()) {
                            addusrschoolcombo.addItem(rs.getString("sname"));
                        }
                        rs.close();
                        pstmt.close();
                    } catch (Exception ex) {
                        //Ignore
                    } finally {
                        try {
                            if (rs != null) rs.close();
                            if (pstmt != null) pstmt.close();
                        } catch (SQLException ign) {
                            // Die in a fire, for all I care
                            // Boo-hoo my ResultSet didn't close properly
                        }
                    }
                } else if (mainpane.getTitleAt(tab).equals("District")) {
                    ResultSet rs = null;
                    dist_textarea.setText("");
                    try {
                        district_combo.removeAllItems();
                        postcodes_combo.removeAllItems();
                        addpcode_combo.removeAllItems();
                        rempcode_dnum_combo.removeAllItems();
                        rename_combo.removeAllItems();
                        String sql = "SELECT * FROM district ORDER BY dnumber ASC";
                        pstmt = conn.prepareStatement(sql);
                        rs = pstmt.executeQuery();
                        while (rs.next()) {
                            dist_textarea.append(rs.getInt("dnumber") + "    " + rs.getString("dname") + "\n");
                            district_combo.addItem(String.valueOf(rs.getInt("dnumber")));
                            postcodes_combo.addItem(String.valueOf(rs.getInt("dnumber")));
                            addpcode_combo.addItem(String.valueOf(rs.getInt("dnumber")));
                            rempcode_dnum_combo.addItem(String.valueOf(rs.getInt("dnumber")));
                            rename_combo.addItem(String.valueOf(rs.getInt("dnumber")));
                        }
                        rs.close();
                        pstmt.close();
                    } catch (Exception ex) {
                        dist_textarea.setText("Error fetching Districts! " + ex.toString());
                        ex.printStackTrace();
                    } finally {
                        try {
                            if (rs != null) rs.close();
                            if (pstmt != null) pstmt.close();
                            //rs = null;
                            //pstmt = null;
                        } catch (SQLException ign) {
                            // Die in a fire, for all I care
                            // Boo-hoo my ResultSet didn't close properly
                        }
                    }
                } else if (mainpane.getTitleAt(tab).equals("School")) {
                    ResultSet rs = null;
                    deps_area.setText("");
                    rem_dep_combo.removeAllItems();
                    alter_dep_id_combo.removeAllItems();
                    add_dep_school_combo.removeAllItems();
                    add_dep_school_combo.setEnabled(false);
                    String sql = "SELECT * FROM Department where schoolname=? ORDER BY depid ASC";
                    try {
                        if (logged.school == null) {
                            sql = "SELECT * FROM Department ORDER BY depid ASC";
                            add_dep_school_combo.setEnabled(true);
                            ResultSet rs2 = null;
                            PreparedStatement pstmt2 = conn.prepareStatement("SELECT sname FROM School");
                            rs2 = pstmt2.executeQuery();
                            while (rs2.next()) {
                                add_dep_school_combo.addItem(rs2.getString("sname"));
                            }
                            rs2.close();
                            pstmt2.close();
                        }            
                        pstmt = conn.prepareStatement(sql);
                        if (logged.school != null) pstmt.setString(1, logged.school);
                        rs = pstmt.executeQuery();
                        deps_area.append(padRight("depid",5) +" "+ padRight("code",5) +" "+ padRight("name",16) +" "+ padRight("budget",5) + "\n");
                        deps_area.append(padRight("-----",5) +" "+ padRight("-----",5) +" "+ padRight("----------------",16) +" "+ padRight("------",5) + "\n");
                        while (rs.next()) {
                            deps_area.append(padRight(String.valueOf(rs.getInt("depid")),5) + " " + 
                                             padRight(rs.getString("code"),5) + " " + 
                                             padRight(rs.getString("name"),16) + " " + 
                                             padRight(String.valueOf(rs.getInt("budget")),5) + "\n");
                            rem_dep_combo.addItem(String.valueOf(rs.getInt("depid")));
                            alter_dep_id_combo.addItem(String.valueOf(rs.getInt("depid")));
                        }
                        rs.close();
                        pstmt.close();
                    } catch (Exception ex) {
                        deps_area.setText("Error fetching Departments! " + ex.toString());
                        ex.printStackTrace();
                    } finally {
                        try {
                            if (rs != null) rs.close();
                            if (pstmt != null) pstmt.close();
                            //rs = null;
                            //pstmt = null;
                        } catch (SQLException ign) {
                            // Die in a fire, for all I care
                            // Boo-hoo my ResultSet didn't close properly
                        }
                    }
                    alter_dep_combo.setSelectedIndex(0);
                } else if (mainpane.getTitleAt(tab).equals("Department")) { 
                    ResultSet rs = null;
                    sel_dep_combo.removeAllItems();
                    String sql = "";
                    if (logged.school == null) {
                        sql = "SELECT depid FROM Department";
                    } else {
                        sql = "SELECT depid FROM Department where schoolname=?";
                    }
                    try {
                        pstmt = conn.prepareStatement(sql);
                        if (logged.school != null) pstmt.setString(1, logged.school);
                        rs = pstmt.executeQuery();
                        while (rs.next()) {
                            sel_dep_combo.addItem(String.valueOf(rs.getInt("depid")));
                        }
                        rs.close();
                        pstmt.close();
                    } catch (Exception ex) {
                        System.out.println("Error fetching departments");
                        ex.printStackTrace();
                    } finally {
                        try {
                            if (rs != null) rs.close();
                            if (pstmt != null) pstmt.close();
                        } catch (Exception ign) {
                            //ignore
                        }
                    }
                } else if (mainpane.getTitleAt(tab).equals("Class")) {
                    ResultSet rs = null;
                    class_seldep_combo.removeAllItems();
                    String sql = "";
                    if (logged.school == null) {
                        sql = "SELECT depid FROM Department";
                    } else {
                        sql = "SELECT depid FROM Department where schoolname=?";
                    }
                    try {
                        pstmt = conn.prepareStatement(sql);
                        if (logged.school != null) pstmt.setString(1, logged.school);
                        rs = pstmt.executeQuery();
                        while (rs.next()) {
                            class_seldep_combo.addItem(String.valueOf(rs.getInt("depid")));
                        }
                        rs.close();
                        pstmt.close();
                    } catch (Exception ex) {
                        System.out.println("Error fetching departments");
                        ex.printStackTrace();
                    } finally {
                        try {
                            if (rs != null) rs.close();
                            if (pstmt != null) pstmt.close();
                        } catch (Exception ign) {
                            //ignore
                        }
                    }
                }
                //-----TAB SWITCHING SQL CODE-----//
            }
        });
        
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        Rectangle bounds = new Rectangle(dim.width/2-this.getSize().width/2,dim.height/2-this.getSize().height/2,800,600);
        this.setMaximizedBounds(bounds);
        
        getInfo();
        infolabel.setText("");
        for (int i = 1; i < components; i++) {
            mainpane.setEnabledAt(i, false);
        }
        loginbutton.setText("Connecting to database...");
        loginbutton.setEnabled(false);
        
        try {
            //Register Oracle Driver
            infolabel.setText("Registering Driver...");
            Class.forName(JDBC_DRIVER);
            
            //Open a Connection
            infolabel.setText("Connecting to Database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            conn.setAutoCommit(true);
        } catch (ClassNotFoundException | SQLException e) {
            infolabel.setText(e.toString());
            err = true;
        }
        
        if (err) {
            loginbutton.setText("Error Connecting to database!");
        } else {
            loginbutton.setText("Log In");
            infolabel.setText("");
            loginbutton.setEnabled(true);
            //resubinfo.setEnabled(false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainpane = new javax.swing.JTabbedPane();
        loginpanel = new javax.swing.JPanel();
        unfield = new javax.swing.JTextField();
        unlabel = new javax.swing.JLabel();
        pwlabel = new javax.swing.JLabel();
        pwfield = new javax.swing.JPasswordField();
        loginbutton = new javax.swing.JButton();
        infolabel = new javax.swing.JLabel();
        resubinfo = new javax.swing.JButton();
        aboutbtn = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        dist_textarea = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        dist_refresh = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        schools_textarea = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        district_combo = new javax.swing.JComboBox<String>();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        postcodestextarea = new javax.swing.JTextArea();
        postcodes_combo = new javax.swing.JComboBox<String>();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        addpcode_combo = new javax.swing.JComboBox<String>();
        jLabel12 = new javax.swing.JLabel();
        addpcode_field = new javax.swing.JTextField();
        addpcode_btn = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        rempcode_dnum_combo = new javax.swing.JComboBox<String>();
        jLabel15 = new javax.swing.JLabel();
        rempcode_pcode_combo = new javax.swing.JComboBox<String>();
        rempcode_btn = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        rename_combo = new javax.swing.JComboBox<String>();
        jLabel18 = new javax.swing.JLabel();
        rename_field = new javax.swing.JTextField();
        rename_btn = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        deps_area = new javax.swing.JTextArea();
        refreshdeps_btn = new javax.swing.JButton();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        add_dep_id_field = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        add_dep_code_field = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        add_dep_name_field = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        add_dep_budget_field = new javax.swing.JTextField();
        add_dep_btn = new javax.swing.JButton();
        jLabel25 = new javax.swing.JLabel();
        rem_dep_combo = new javax.swing.JComboBox<String>();
        rem_dep_btn = new javax.swing.JButton();
        jLabel26 = new javax.swing.JLabel();
        alter_dep_combo = new javax.swing.JComboBox();
        alter_dep_btn = new javax.swing.JButton();
        alter_dep_field = new javax.swing.JTextField();
        alter_dep_id_combo = new javax.swing.JComboBox<String>();
        add_dep_school_combo = new javax.swing.JComboBox<String>();
        jLabel27 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        sel_dep_combo = new javax.swing.JComboBox<String>();
        jLabel29 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        classes_area = new javax.swing.JTextArea();
        jLabel30 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        teachers_area = new javax.swing.JTextArea();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        add_class_num_field = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        add_class_name_field = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        add_class_yr_field = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        add_class_teacher_combo = new javax.swing.JComboBox<String>();
        add_class_btn = new javax.swing.JButton();
        jLabel36 = new javax.swing.JLabel();
        rem_class_combo = new javax.swing.JComboBox<String>();
        jButton1 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        class_seldep_combo = new javax.swing.JComboBox<String>();
        jLabel38 = new javax.swing.JLabel();
        class_selclass_combo = new javax.swing.JComboBox<String>();
        jLabel39 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        class_student_area = new javax.swing.JTextArea();
        jLabel41 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        class_grades_area = new javax.swing.JTextArea();
        jLabel40 = new javax.swing.JLabel();
        jScrollPane10 = new javax.swing.JScrollPane();
        fgrade_area = new javax.swing.JTextArea();
        fgrade_btn = new javax.swing.JButton();
        fgrade_field = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        admin_textarea = new javax.swing.JTextArea();
        adminfield = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        addusrfield = new javax.swing.JTextField();
        addusrpwfield = new javax.swing.JPasswordField();
        addusrschoolcombo = new javax.swing.JComboBox<String>();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        addusrpermcombo = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        addusrbtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SchoolDB");
        setFont(new java.awt.Font("Verdana", 0, 10)); // NOI18N
        setMaximumSize(new java.awt.Dimension(800, 600));
        setMinimumSize(new java.awt.Dimension(800, 600));
        setName("mainform"); // NOI18N
        setResizable(false);

        mainpane.setTabPlacement(javax.swing.JTabbedPane.LEFT);
        mainpane.setPreferredSize(new java.awt.Dimension(800, 600));

        unfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                unfieldActionPerformed(evt);
            }
        });

        unlabel.setText("Username:");

        pwlabel.setText("Password:");

        pwfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pwfieldActionPerformed(evt);
            }
        });

        loginbutton.setText("Login");
        loginbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginbuttonActionPerformed(evt);
            }
        });

        infolabel.setText("jLabel1");

        resubinfo.setText("Resubmit Database Info");
        resubinfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resubinfoActionPerformed(evt);
            }
        });

        aboutbtn.setText("About");
        aboutbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutbtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout loginpanelLayout = new javax.swing.GroupLayout(loginpanel);
        loginpanel.setLayout(loginpanelLayout);
        loginpanelLayout.setHorizontalGroup(
            loginpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loginpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(loginpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(infolabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(loginpanelLayout.createSequentialGroup()
                        .addGroup(loginpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(unlabel)
                            .addComponent(pwlabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(loginpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(resubinfo, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                            .addComponent(pwfield)
                            .addComponent(unfield)
                            .addComponent(loginbutton, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE))
                        .addGap(0, 461, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginpanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(aboutbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        loginpanelLayout.setVerticalGroup(
            loginpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loginpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(loginpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(unlabel)
                    .addComponent(unfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(loginpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pwfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pwlabel))
                .addGap(18, 18, 18)
                .addComponent(loginbutton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(resubinfo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(infolabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 402, Short.MAX_VALUE)
                .addComponent(aboutbtn)
                .addContainerGap())
        );

        mainpane.addTab("Login", loginpanel);
        loginpanel.getAccessibleContext().setAccessibleName("");

        dist_textarea.setEditable(false);
        dist_textarea.setColumns(20);
        dist_textarea.setRows(5);
        dist_textarea.setTabSize(4);
        jScrollPane1.setViewportView(dist_textarea);

        jLabel1.setText("Districts");

        dist_refresh.setText("Refresh Districts");
        dist_refresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dist_refreshActionPerformed(evt);
            }
        });

        schools_textarea.setEditable(false);
        schools_textarea.setColumns(20);
        schools_textarea.setRows(5);
        jScrollPane3.setViewportView(schools_textarea);

        jLabel3.setText("Schools in District");

        district_combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                district_comboActionPerformed(evt);
            }
        });

        jLabel9.setText("Postcodes in District");

        postcodestextarea.setEditable(false);
        postcodestextarea.setColumns(5);
        postcodestextarea.setRows(5);
        jScrollPane4.setViewportView(postcodestextarea);

        postcodes_combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                postcodes_comboActionPerformed(evt);
            }
        });

        jLabel10.setText("Add Postcode");

        jLabel11.setText("District:");

        jLabel12.setText("Postcode:");

        addpcode_btn.setText("Add");
        addpcode_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addpcode_btnActionPerformed(evt);
            }
        });

        jLabel13.setText("Remove Postcode");

        jLabel14.setText("District:");

        rempcode_dnum_combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rempcode_dnum_comboActionPerformed(evt);
            }
        });

        jLabel15.setText("Postcode:");

        rempcode_btn.setText("Remove");
        rempcode_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rempcode_btnActionPerformed(evt);
            }
        });

        jLabel16.setText("Rename District");

        jLabel17.setText("District:");

        rename_combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rename_comboActionPerformed(evt);
            }
        });

        jLabel18.setText("New name:");

        rename_btn.setText("Rename");
        rename_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rename_btnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(dist_refresh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 723, Short.MAX_VALUE)
                    .addComponent(district_combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3)
                            .addComponent(jScrollPane4)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(postcodes_combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12)
                            .addComponent(addpcode_combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(addpcode_field)
                            .addComponent(addpcode_btn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(rempcode_pcode_combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(rempcode_btn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel15)
                            .addComponent(jLabel14)
                            .addComponent(rempcode_dnum_combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .addComponent(jLabel17)
                            .addComponent(rename_combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel18)
                            .addComponent(rename_field)
                            .addComponent(rename_btn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dist_refresh)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(district_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel13)
                    .addComponent(jLabel16))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(jLabel14)
                            .addComponent(jLabel17))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(addpcode_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rempcode_dnum_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rename_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(jLabel15)
                            .addComponent(jLabel18))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(addpcode_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rempcode_pcode_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rename_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(addpcode_btn)
                            .addComponent(rempcode_btn)
                            .addComponent(rename_btn))
                        .addGap(0, 89, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(postcodes_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        mainpane.addTab("District", jPanel2);

        jLabel19.setText("Departments in your school");

        deps_area.setEditable(false);
        deps_area.setColumns(20);
        deps_area.setRows(5);
        jScrollPane5.setViewportView(deps_area);

        refreshdeps_btn.setText("Refresh Departments");
        refreshdeps_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshdeps_btnActionPerformed(evt);
            }
        });

        jLabel20.setText("Add Department");

        jLabel21.setText("ID:");

        jLabel22.setText("Code:");

        jLabel23.setText("Name:");

        jLabel24.setText("Budget:");

        add_dep_btn.setText("Add");
        add_dep_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                add_dep_btnActionPerformed(evt);
            }
        });

        jLabel25.setText("Remove Department");

        rem_dep_btn.setText("Remove");
        rem_dep_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rem_dep_btnActionPerformed(evt);
            }
        });

        jLabel26.setText("Alter Department");

        alter_dep_combo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Code", "Name", "Budget" }));
        alter_dep_combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                alter_dep_comboActionPerformed(evt);
            }
        });

        alter_dep_btn.setText("Alter");
        alter_dep_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                alter_dep_btnActionPerformed(evt);
            }
        });

        alter_dep_id_combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                alter_dep_id_comboActionPerformed(evt);
            }
        });

        jLabel27.setText("School:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5)
                    .addComponent(refreshdeps_btn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel19)
                            .addComponent(jLabel20)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel26, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(add_dep_id_field, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel21, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel25, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                    .addComponent(rem_dep_combo, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(alter_dep_id_combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(add_dep_code_field)
                                    .addComponent(rem_dep_btn, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                    .addComponent(alter_dep_combo, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                    .addComponent(add_dep_name_field)
                                    .addComponent(alter_dep_field, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(add_dep_budget_field)
                                    .addComponent(alter_dep_btn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(add_dep_school_combo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(add_dep_btn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel27))))
                        .addGap(0, 93, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(refreshdeps_btn)
                .addGap(18, 18, 18)
                .addComponent(jLabel20)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(jLabel22)
                    .addComponent(jLabel23)
                    .addComponent(jLabel24)
                    .addComponent(jLabel27))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(add_dep_id_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(add_dep_code_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(add_dep_name_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(add_dep_budget_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(add_dep_btn)
                    .addComponent(add_dep_school_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel25)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rem_dep_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rem_dep_btn))
                .addGap(18, 18, 18)
                .addComponent(jLabel26)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(alter_dep_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(alter_dep_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(alter_dep_btn)
                    .addComponent(alter_dep_id_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(204, Short.MAX_VALUE))
        );

        mainpane.addTab("School", jPanel3);

        jLabel28.setText("Select Department:");

        sel_dep_combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sel_dep_comboActionPerformed(evt);
            }
        });

        jLabel29.setText("Classes in Department");

        classes_area.setEditable(false);
        classes_area.setColumns(20);
        classes_area.setRows(5);
        jScrollPane6.setViewportView(classes_area);

        jLabel30.setText("Teachers in Department");

        teachers_area.setColumns(20);
        teachers_area.setRows(5);
        jScrollPane7.setViewportView(teachers_area);

        jLabel31.setText("Add Class");

        jLabel32.setText("Number:");

        jLabel33.setText("Name:");

        jLabel34.setText("Year:");

        jLabel35.setText("Teacher:");

        add_class_btn.setText("Add");
        add_class_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                add_class_btnActionPerformed(evt);
            }
        });

        jLabel36.setText("Remove Class");

        jButton1.setText("Remove");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6)
                    .addComponent(jScrollPane7)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel28)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(sel_dep_combo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel29)
                            .addComponent(jLabel30)
                            .addComponent(jLabel36)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(rem_class_combo, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel31, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                    .addComponent(jLabel32, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(add_class_num_field, javax.swing.GroupLayout.Alignment.LEADING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(add_class_name_field, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(add_class_yr_field, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(add_class_teacher_combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(add_class_btn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 199, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(sel_dep_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel29)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel30)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel31)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32)
                    .addComponent(jLabel33)
                    .addComponent(jLabel34)
                    .addComponent(jLabel35))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(add_class_num_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(add_class_name_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(add_class_yr_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(add_class_teacher_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(add_class_btn))
                .addGap(18, 18, 18)
                .addComponent(jLabel36)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rem_class_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addContainerGap(90, Short.MAX_VALUE))
        );

        mainpane.addTab("Department", jPanel1);

        jLabel37.setText("Select Department:");

        class_seldep_combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                class_seldep_comboActionPerformed(evt);
            }
        });

        jLabel38.setText("Select Class:");

        class_selclass_combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                class_selclass_comboActionPerformed(evt);
            }
        });

        jLabel39.setText("Students in selected Class:");

        class_student_area.setEditable(false);
        class_student_area.setColumns(20);
        class_student_area.setRows(5);
        jScrollPane8.setViewportView(class_student_area);

        jLabel41.setText("Student Grades in selected Class:");

        class_grades_area.setEditable(false);
        class_grades_area.setColumns(20);
        class_grades_area.setRows(5);
        jScrollPane9.setViewportView(class_grades_area);

        jLabel40.setText("Filter Students by Average Grade (in all Classes):");

        fgrade_area.setColumns(20);
        fgrade_area.setRows(5);
        jScrollPane10.setViewportView(fgrade_area);

        fgrade_btn.setText("Filter");
        fgrade_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fgrade_btnActionPerformed(evt);
            }
        });

        jLabel42.setText("Grade:");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8)
                    .addComponent(jScrollPane9)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel37)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(class_seldep_combo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel38)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(class_selclass_combo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel39)
                            .addComponent(jLabel41)
                            .addComponent(jLabel40))
                        .addGap(0, 322, Short.MAX_VALUE))
                    .addComponent(jScrollPane10)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel42)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fgrade_field, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fgrade_btn, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel37)
                    .addComponent(class_seldep_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel38)
                    .addComponent(class_selclass_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel39)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel41)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel40)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fgrade_btn)
                    .addComponent(fgrade_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel42))
                .addContainerGap(37, Short.MAX_VALUE))
        );

        mainpane.addTab("Class", jPanel4);

        jLabel2.setText("Admin Console");

        admin_textarea.setEditable(false);
        admin_textarea.setColumns(20);
        admin_textarea.setRows(5);
        jScrollPane2.setViewportView(admin_textarea);

        adminfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adminfieldActionPerformed(evt);
            }
        });

        jLabel4.setText("Add User");

        addusrfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addusrfieldActionPerformed(evt);
            }
        });

        addusrpwfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addusrpwfieldActionPerformed(evt);
            }
        });

        jLabel5.setText("Username:");

        jLabel6.setText("Password:");

        jLabel7.setText("School:");

        addusrpermcombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "A", "D", "S", "E", "C" }));

        jLabel8.setText("Permission:");

        addusrbtn.setText("Add User");
        addusrbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addusrbtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                            .addComponent(addusrfield))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(addusrpwfield, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                            .addComponent(addusrschoolcombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(addusrpermcombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addusrbtn, javax.swing.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE))
                    .addComponent(jScrollPane2)
                    .addComponent(adminfield))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(adminfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(addusrfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel5Layout.createSequentialGroup()
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel6)
                                .addComponent(jLabel5))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(addusrpwfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(addusrschoolcombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(addusrpermcombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(addusrbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(205, Short.MAX_VALUE))
        );

        mainpane.addTab("Admin", jPanel5);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainpane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainpane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void loginbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginbuttonActionPerformed
        int components = mainpane.getComponentCount();
        if (loginbutton.getText().equals("Log In")) {
            String user = unfield.getText();
            String pw = String.valueOf(pwfield.getPassword());
            boolean err = true;
            try {
                String sql = "SELECT * FROM users WHERE uname = ?";
                pstmt = conn.prepareStatement(sql);
                pstmt.setString(1, user);
                ResultSet rs = pstmt.executeQuery();
                while(rs.next()) {
                    if(rs.getString("passw").equals(pw)) {
                        err = false;
                        unfield.setEnabled(false);
                        pwfield.setEnabled(false);
                        loginbutton.setText("Log Out");
                        infolabel.setText("");
                        logged.uname = user;
                        logged.perms = rs.getString("perms");
                        logged.school = rs.getString("school");
                        System.out.println(logged.toString());
                    }
                    break;
                }
                if (err) {
                    infolabel.setText("Username/Password invalid");
                    unfield.setText("");
                    pwfield.setText("");
                }
            } catch (Exception e) {
                infolabel.setText(e.toString());
                unfield.setEnabled(true);
                pwfield.setEnabled(true);
                unfield.setText("");
                pwfield.setText("");
                loginbutton.setText("Log In");
                logged = new User();
            }
        } else {
            unfield.setEnabled(true);
            pwfield.setEnabled(true);
            unfield.setText("");
            pwfield.setText("");
            loginbutton.setText("Log In");
            logged = new User();
        }
        
        //Disable all tabs
        for (int i = 1; i < components; i++) {
            mainpane.setEnabledAt(i, false);
        }
        
        //Now set them based on permissions
        switch (logged.perms) {
            case "D":
                mainpane.setEnabledAt(mainpane.indexOfTab("District"), true);
                mainpane.setEnabledAt(mainpane.indexOfTab("School"), true);
                mainpane.setEnabledAt(mainpane.indexOfTab("Department"), true);
                mainpane.setEnabledAt(mainpane.indexOfTab("Class"), true);
                break;
            case "S":
                mainpane.setEnabledAt(mainpane.indexOfTab("School"), true);
                mainpane.setEnabledAt(mainpane.indexOfTab("Department"), true);
                mainpane.setEnabledAt(mainpane.indexOfTab("Class"), true);
                break;
            case "E":
                mainpane.setEnabledAt(mainpane.indexOfTab("Department"), true);
                mainpane.setEnabledAt(mainpane.indexOfTab("Class"), true);
                break;
            case "C":
                mainpane.setEnabledAt(mainpane.indexOfTab("Class"), true);
                break;
            case "A":
                mainpane.setEnabledAt(mainpane.indexOfTab("District"), true);
                mainpane.setEnabledAt(mainpane.indexOfTab("School"), true);
                mainpane.setEnabledAt(mainpane.indexOfTab("Department"), true);
                mainpane.setEnabledAt(mainpane.indexOfTab("Class"), true);
                mainpane.setEnabledAt(mainpane.indexOfTab("Admin"), true);
                break;
        }
    }//GEN-LAST:event_loginbuttonActionPerformed

    private void dist_refreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dist_refreshActionPerformed
        ResultSet rs = null;
        dist_textarea.setText("");
        try {
            district_combo.removeAllItems();
            postcodes_combo.removeAllItems();
            addpcode_combo.removeAllItems();
            rempcode_dnum_combo.removeAllItems();
            rename_combo.removeAllItems();
            String sql = "SELECT * FROM district ORDER BY dnumber ASC";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                dist_textarea.append(rs.getInt("dnumber") + "    " + rs.getString("dname") + "\n");
                district_combo.addItem(String.valueOf(rs.getInt("dnumber")));
                postcodes_combo.addItem(String.valueOf(rs.getInt("dnumber")));
                addpcode_combo.addItem(String.valueOf(rs.getInt("dnumber")));
                rempcode_dnum_combo.addItem(String.valueOf(rs.getInt("dnumber")));
                rename_combo.addItem(String.valueOf(rs.getInt("dnumber")));
            }
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            dist_textarea.setText("Error fetching Districts! " + e.toString());
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
            } catch (SQLException ign) {
                // Die in a fire, for all I care
                // Boo-hoo my ResultSet didn't close properly
            }
        }
    }//GEN-LAST:event_dist_refreshActionPerformed

    private void adminfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adminfieldActionPerformed
        String input = adminfield.getText();
        admin_textarea.setText("");
        boolean err = true;
        adminfield.setText("");
        ResultSet rs = null;
        ResultSetMetaData rsmd = null;
        Statement stmt = null;
        int cols = 0;
        int rows = 0;
        
        if (input.indexOf(';') != -1) {
            admin_textarea.setText("Input must not contain semicolons.");
        } else if (input.toLowerCase().equals("cls")) {
            admin_textarea.setText("");
        } else if (input.toLowerCase().equals("commit")) {
            admin_textarea.setText("Changes Committed.");
            try {
                conn.commit();
            } catch (SQLException ex) {
                admin_textarea.setText("Error: " + ex.toString());
            }
        } else {
            StringBuilder data = new StringBuilder();
            try {
                stmt = conn.createStatement();
                stmt.execute(input);
                rs = stmt.getResultSet();
                try {
                    rsmd = rs.getMetaData();
                    cols = rsmd.getColumnCount();
                } catch (Exception ign) {
                    //This throws an exception
                    //If we do anything that doesn't return anything
                }
                if (rs != null) {
                    while (rs.next()) {
                        err = false;
                        rows++;
                        if (rows > 1) {
                            data.append("\n\n");
                        }
                        for (int col = 1; col <= cols; col++) {
                            if (col > 1) {
                                data.append("    ");
                            }
                            String currCol = rs.getString(col);
                            data.append(currCol);
                        }
                    }
                    rs.close();
                    stmt.close();
                }
                if (data.length() > 0) {
                    admin_textarea.setText(data.toString());
                } else {
                    admin_textarea.setText(String.valueOf(stmt.getUpdateCount()) + " row(s) modified");
                }
            } catch (SQLException e) {
                admin_textarea.setText("Error: " + e.toString());
            } finally {
                if (rs != null) try {
                    rs.close();
                    stmt.close();
                    rs = null;
                    rsmd = null;
                    stmt = null;
                } catch (SQLException ign) {
                    // Ignore
                }
            }
        }
        mainpane.setSelectedIndex(mainpane.indexOfTab("Login"));
        mainpane.setSelectedIndex(mainpane.indexOfTab("Admin"));
    }//GEN-LAST:event_adminfieldActionPerformed

    private void pwfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pwfieldActionPerformed
        loginbutton.doClick();
    }//GEN-LAST:event_pwfieldActionPerformed

    private void unfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_unfieldActionPerformed
        loginbutton.doClick();
    }//GEN-LAST:event_unfieldActionPerformed

    private void district_comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_district_comboActionPerformed
        ResultSet rs = null;
        schools_textarea.setText("");
        try {
            String sql = "SELECT * FROM School WHERE dnum = ? ORDER BY sname ASC";
            pstmt = conn.prepareStatement(sql);
            try {
                pstmt.setInt(1, Integer.parseInt(district_combo.getSelectedItem().toString()));
            } catch (NumberFormatException | SQLException | NullPointerException ex) {
                pstmt.setInt(1, 0);
            }
            rs = pstmt.executeQuery();
            while (rs.next()) {
                schools_textarea.append(rs.getString("sname") + "    " + rs.getString("type") + "    " + rs.getString("addr") + "\n");
            }
            rs.close();
            pstmt.close();
        } catch (SQLException | NumberFormatException e) {
            schools_textarea.setText("Error fetching Schools!\n" + e.toString());
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
            } catch (SQLException ign) {
                // Die in a fire, for all I care
                // Boo-hoo my ResultSet didn't close properly
            }
        }
    }//GEN-LAST:event_district_comboActionPerformed

    private void addusrfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addusrfieldActionPerformed
        addusrbtn.doClick();
    }//GEN-LAST:event_addusrfieldActionPerformed

    private void addusrpwfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addusrpwfieldActionPerformed
        addusrbtn.doClick();
    }//GEN-LAST:event_addusrpwfieldActionPerformed

    private void addusrbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addusrbtnActionPerformed
        String uname = addusrfield.getText();
        String passw = String.valueOf(addusrpwfield.getPassword());
        String school = addusrschoolcombo.getSelectedItem().toString();
        String perms = addusrpermcombo.getSelectedItem().toString();
        try {
            String sql = "INSERT INTO USERS VALUES (?, ?, ?, ?)";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, uname);
            pstmt.setString(2, passw);
            pstmt.setString(3, perms);
            pstmt.setString(4, school);
            pstmt.executeUpdate();
            conn.commit();
            JOptionPane.showMessageDialog(mainpane, "Added '" + uname + "' successfully!");
        } catch (SQLException | HeadlessException e) {
            JOptionPane.showMessageDialog(mainpane, "Error executing query: " + e.toString());
        }
        addusrfield.setText("");
        addusrpwfield.setText("");
        addusrschoolcombo.setSelectedIndex(0);
        addusrpermcombo.setSelectedIndex(0);
    }//GEN-LAST:event_addusrbtnActionPerformed

    private void resubinfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resubinfoActionPerformed
        if (loginbutton.getText().equals("Log Out")) {
            loginbutton.doClick();
        }
        
        int components = mainpane.getComponentCount();
        boolean err = false;
        getInfo();
        infolabel.setText("");
        for (int i = 1; i < components; i++) {
            mainpane.setEnabledAt(i, false);
        }
        loginbutton.setText("Connecting to database...");
        loginbutton.setEnabled(false);
        
        try {
            //Register Oracle Driver
            infolabel.setText("Registering Driver...");
            Class.forName(JDBC_DRIVER);
            
            //Open a Connection
            infolabel.setText("Connecting to Database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (ClassNotFoundException | SQLException e) {
            infolabel.setText(e.toString());
            err = true;
        }
        
        if (err) {
            loginbutton.setText("Error Connecting to database!");
        } else {
            loginbutton.setText("Log In");
            infolabel.setText("");
            loginbutton.setEnabled(true);
            //resubinfo.setEnabled(false);
        }
    }//GEN-LAST:event_resubinfoActionPerformed

    private void postcodes_comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_postcodes_comboActionPerformed
        ResultSet rs = null;
        postcodestextarea.setText("");
        try {
            String sql = "SELECT * FROM Postcodes WHERE dnum = ? ORDER BY dnum ASC";
            pstmt = conn.prepareStatement(sql);
            try {
                pstmt.setInt(1, Integer.parseInt(postcodes_combo.getSelectedItem().toString()));
            } catch (NumberFormatException | SQLException | NullPointerException ex) {
                pstmt.setInt(1, 0);
            }
            rs = pstmt.executeQuery();
            while (rs.next()) {
                postcodestextarea.append(rs.getInt("dnum") + "    " + rs.getInt("pcode") + "\n");
            }
            rs.close();
            pstmt.close();
        } catch (SQLException | NumberFormatException e) {
            postcodestextarea.setText("Error fetching Postcodes!\n" + e.toString());
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
            } catch (SQLException ign) {
                // Die in a fire, for all I care
                // Boo-hoo my ResultSet didn't close properly
            }
        }
    }//GEN-LAST:event_postcodes_comboActionPerformed

    private void addpcode_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addpcode_btnActionPerformed
        try {
            int pcode = Integer.valueOf(addpcode_field.getText());
            int dnum = Integer.valueOf(addpcode_combo.getSelectedItem().toString());
            try {
                String sql = "INSERT INTO Postcodes VALUES (?, ?)";
                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, pcode);
                pstmt.setInt(2, dnum);
                pstmt.executeUpdate();
                conn.commit();
                JOptionPane.showMessageDialog(mainpane, "Added Postcode '" + pcode + "' to District '" + dnum + "' successfully!");
            } catch (SQLException | HeadlessException e) {
                JOptionPane.showMessageDialog(mainpane, "Error executing query: " + e.toString());
            }
        addpcode_field.setText("");
        addpcode_combo.setSelectedIndex(0);    
        dist_refresh.doClick();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(mainpane, "Error executing: " + e.toString());
        }
    }//GEN-LAST:event_addpcode_btnActionPerformed

    private void rempcode_dnum_comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rempcode_dnum_comboActionPerformed
        ResultSet rs = null;
        rempcode_pcode_combo.removeAllItems();
        try {
            String sql = "SELECT * FROM Postcodes WHERE dnum = ? ORDER BY dnum ASC";
            pstmt = conn.prepareStatement(sql);
            try {
                pstmt.setInt(1, Integer.parseInt(rempcode_dnum_combo.getSelectedItem().toString()));
            } catch (NumberFormatException | SQLException | NullPointerException ex) {
                pstmt.setInt(1, 0);
            }
            rs = pstmt.executeQuery();
            while (rs.next()) {
                rempcode_pcode_combo.addItem(String.valueOf(rs.getInt("pcode")));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException | NumberFormatException e) {
            JOptionPane.showMessageDialog(mainpane, "Error executing: " + e.toString());
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
            } catch (SQLException ign) {
                // Die in a fire, for all I care
                // Boo-hoo my ResultSet didn't close properly
            }
        }
    }//GEN-LAST:event_rempcode_dnum_comboActionPerformed

    private void rempcode_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rempcode_btnActionPerformed
        try {
            int pcode = Integer.valueOf(rempcode_pcode_combo.getSelectedItem().toString());
            int dnum = Integer.valueOf(rempcode_dnum_combo.getSelectedItem().toString());
            try {
                String sql = "DELETE FROM Postcodes WHERE dnum = ? AND pcode = ?";
                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, dnum);
                pstmt.setInt(2, pcode);
                pstmt.executeUpdate();
                conn.commit();
                JOptionPane.showMessageDialog(mainpane, "Deleted Postcode '" + pcode + "' from District '" + dnum + "' successfully!");
            } catch (SQLException | HeadlessException e) {
                JOptionPane.showMessageDialog(mainpane, "Error executing query: " + e.toString());
            }
        rempcode_pcode_combo.setSelectedIndex(0);
        rempcode_dnum_combo.setSelectedIndex(0);
        dist_refresh.doClick();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(mainpane, "Error executing: " + e.toString());
        }
    }//GEN-LAST:event_rempcode_btnActionPerformed

    private void rename_comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rename_comboActionPerformed
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM District WHERE dnumber = ? ORDER BY dnumber ASC";
            pstmt = conn.prepareStatement(sql);
            try {
                pstmt.setInt(1, Integer.parseInt(rename_combo.getSelectedItem().toString()));
            } catch (NumberFormatException | SQLException | NullPointerException ex) {
                pstmt.setInt(1, 0);
            }
            rs = pstmt.executeQuery();
            while (rs.next()) {
                rename_field.setText(rs.getString("dname"));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException | NumberFormatException e) {
            JOptionPane.showMessageDialog(mainpane, "Error executing: " + e.toString());
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
            } catch (SQLException ign) {
                // Die in a fire, for all I care
                // Boo-hoo my ResultSet didn't close properly
            }
        }
    }//GEN-LAST:event_rename_comboActionPerformed

    private void rename_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rename_btnActionPerformed
        ResultSet rs = null;
        try {
            String sql = "UPDATE District SET dname=? WHERE dnumber=?";
            pstmt = conn.prepareStatement(sql);
            try {
                pstmt.setString(1, rename_field.getText());
                pstmt.setInt(2, Integer.parseInt(rename_combo.getSelectedItem().toString()));
            } catch (NumberFormatException | SQLException | NullPointerException ex) {
                pstmt.setString(1, "");
                pstmt.setInt(2, 0);
            }
            rs = pstmt.executeQuery();
            /*while (rs.next()) {
                rename_field.setText(rs.getString("dname"));
            }*/
            rs.close();
            pstmt.close();
        } catch (SQLException | NumberFormatException e) {
            JOptionPane.showMessageDialog(mainpane, "Error executing: " + e.toString());
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
            } catch (SQLException ign) {
                // Die in a fire, for all I care
                // Boo-hoo my ResultSet didn't close properly
            }
        }
        dist_refresh.doClick();
    }//GEN-LAST:event_rename_btnActionPerformed

    private void refreshdeps_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshdeps_btnActionPerformed
        ResultSet rs = null;
        deps_area.setText("");
        rem_dep_combo.removeAllItems();
        alter_dep_id_combo.removeAllItems();
        add_dep_school_combo.removeAllItems();
        add_dep_school_combo.setEnabled(false);
        String sql = "SELECT * FROM Department where schoolname=? ORDER BY depid ASC";
        try {
            if (logged.school == null) {
                sql = "SELECT * FROM Department ORDER BY depid ASC";
                add_dep_school_combo.setEnabled(true);
                ResultSet rs2 = null;
                PreparedStatement pstmt2 = conn.prepareStatement("SELECT sname FROM School");
                rs2 = pstmt2.executeQuery();
                while (rs2.next()) {
                    add_dep_school_combo.addItem(rs2.getString("sname"));
                }
                rs2.close();
                pstmt2.close();
            }            
            pstmt = conn.prepareStatement(sql);
            if (logged.school != null) pstmt.setString(1, logged.school);
            rs = pstmt.executeQuery();
            deps_area.append(padRight("depid",5) +" "+ padRight("code",5) +" "+ padRight("name",16) +" "+ padRight("budget",5) + "\n");
            deps_area.append(padRight("-----",5) +" "+ padRight("-----",5) +" "+ padRight("----------------",16) +" "+ padRight("------",5) + "\n");
            while (rs.next()) {
                deps_area.append(padRight(String.valueOf(rs.getInt("depid")),5) + " " + 
                                 padRight(rs.getString("code"),5) + " " + 
                                 padRight(rs.getString("name"),16) + " " + 
                                 padRight(String.valueOf(rs.getInt("budget")),5) + "\n");
                rem_dep_combo.addItem(String.valueOf(rs.getInt("depid")));
                alter_dep_id_combo.addItem(String.valueOf(rs.getInt("depid")));
            }
            rs.close();
            pstmt.close();
        } catch (Exception ex) {
            deps_area.setText("Error fetching Departments! " + ex.toString());
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
                //rs = null;
                //pstmt = null;
            } catch (SQLException ign) {
                // Die in a fire, for all I care
                // Boo-hoo my ResultSet didn't close properly
            }
        }
        alter_dep_combo.setSelectedIndex(0);
    }//GEN-LAST:event_refreshdeps_btnActionPerformed

    private void rem_dep_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rem_dep_btnActionPerformed
        ResultSet rs = null;
        String sql = "DELETE FROM Department where depid=?";
        try {      
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, Integer.valueOf(rem_dep_combo.getSelectedItem().toString()));
            rs = pstmt.executeQuery();
            rs.close();
            pstmt.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(mainpane, "Error executing: " + ex.toString());
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
                //rs = null;
                //pstmt = null;
            } catch (SQLException ign) {
                // Die in a fire, for all I care
                // Boo-hoo my ResultSet didn't close properly
            }
        }
        refreshdeps_btn.doClick();
    }//GEN-LAST:event_rem_dep_btnActionPerformed

    private void alter_dep_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_alter_dep_btnActionPerformed
        ResultSet rs = null;
        String sql = "";
        try {      
            if (alter_dep_combo.getSelectedIndex() == 0) {
                sql = "UPDATE Department SET code=? WHERE depid=?";
                pstmt = conn.prepareStatement(sql);
                pstmt.setString(1, alter_dep_field.getText());
                pstmt.setInt(2, Integer.valueOf(alter_dep_id_combo.getSelectedItem().toString()));
            } else if (alter_dep_combo.getSelectedIndex() == 1) {
                sql = "UPDATE Department SET name=? WHERE depid=?";
                pstmt = conn.prepareStatement(sql);
                pstmt.setString(1, alter_dep_field.getText());
                pstmt.setInt(2, Integer.valueOf(alter_dep_id_combo.getSelectedItem().toString()));
            } else {
                sql = "UPDATE Department SET budget=? WHERE depid=?";
                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, Integer.valueOf(alter_dep_field.getText()));
                pstmt.setInt(2, Integer.valueOf(alter_dep_id_combo.getSelectedItem().toString()));
            }
            rs = pstmt.executeQuery();
            rs.close();
            pstmt.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(mainpane, "Error executing: " + ex.toString());
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
                //rs = null;
                //pstmt = null;
            } catch (SQLException ign) {
                // Die in a fire, for all I care
                // Boo-hoo my ResultSet didn't close properly
            }
        }
        refreshdeps_btn.doClick();
    }//GEN-LAST:event_alter_dep_btnActionPerformed

    private void alter_dep_comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_alter_dep_comboActionPerformed
        if (alter_dep_id_combo.getSelectedItem() == null) {
            //ignore
        } else {
            ResultSet rs = null;
            String sql = "";
            try {      
                if (alter_dep_combo.getSelectedIndex() == 0) {
                    sql = "SELECT code FROM Department WHERE depid=?";
                    pstmt = conn.prepareStatement(sql);
                    pstmt.setInt(1, Integer.valueOf(alter_dep_id_combo.getSelectedItem().toString()));
                } else if (alter_dep_combo.getSelectedIndex() == 1) {
                    sql = "SELECT name FROM Department WHERE depid=?";
                    pstmt = conn.prepareStatement(sql);
                    pstmt.setInt(1, Integer.valueOf(alter_dep_id_combo.getSelectedItem().toString()));
                } else {
                    sql = "SELECT budget FROM Department WHERE depid=?";
                    pstmt = conn.prepareStatement(sql);
                    pstmt.setInt(1, Integer.valueOf(alter_dep_id_combo.getSelectedItem().toString()));
                }
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    if (alter_dep_combo.getSelectedIndex() == 0) {
                        alter_dep_field.setText(rs.getString("code"));
                    } else if (alter_dep_combo.getSelectedIndex() == 1) {
                        alter_dep_field.setText(rs.getString("name"));
                    } else {
                        alter_dep_field.setText(String.valueOf(rs.getInt("budget")));
                    }
                }
                rs.close();
                pstmt.close();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(mainpane, "Error executing: " + ex.toString());
                ex.printStackTrace();
            } finally {
                try {
                    if (rs != null) rs.close();
                    if (pstmt != null) pstmt.close();
                    //rs = null;
                    //pstmt = null;
                } catch (SQLException ign) {
                    // Die in a fire, for all I care
                    // Boo-hoo my ResultSet didn't close properly
                }
            }
        }
    }//GEN-LAST:event_alter_dep_comboActionPerformed

    private void alter_dep_id_comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_alter_dep_id_comboActionPerformed
        alter_dep_combo.setSelectedIndex(0);
    }//GEN-LAST:event_alter_dep_id_comboActionPerformed

    private void add_dep_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_add_dep_btnActionPerformed
        ResultSet rs = null;
        String sql = "INSERT INTO Department VALUES (?, ?, ?, ?, ?)";
        boolean err = false;
        int depid = 0;
        String code = "";
        String name = "";
        int budget = 0;
        String school = "";
        try {
            depid = Integer.valueOf(add_dep_id_field.getText());
            code = add_dep_code_field.getText();
            name = add_dep_name_field.getText();
            budget = Integer.valueOf(add_dep_budget_field.getText());
            if (logged.school == null) {
                school = add_dep_school_combo.getSelectedItem().toString();
            } else {
                school = logged.school;
            }
        } catch (Exception e) {
            err = true;
            JOptionPane.showMessageDialog(mainpane, "Error executing: " + e.toString());
        }
        if (err) {
            //error, do nothing
        } else {
            try {      
                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, depid);
                pstmt.setString(2, code);
                pstmt.setString(3, name);
                pstmt.setInt(4, budget);
                pstmt.setString(5, school);
                rs = pstmt.executeQuery();
                rs.close();
                pstmt.close();
                JOptionPane.showMessageDialog(mainpane, "Department '" + depid + "' added successfully!");
                add_dep_id_field.setText("");
                add_dep_code_field.setText("");
                add_dep_name_field.setText("");
                add_dep_budget_field.setText("");
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(mainpane, "Error executing: " + ex.toString());
                ex.printStackTrace();
            } finally {
                try {
                    if (rs != null) rs.close();
                    if (pstmt != null) pstmt.close();
                    //rs = null;
                    //pstmt = null;
                } catch (SQLException ign) {
                    // Die in a fire, for all I care
                    // Boo-hoo my ResultSet didn't close properly
                }
            }
            refreshdeps_btn.doClick();
        }
    }//GEN-LAST:event_add_dep_btnActionPerformed

    private void sel_dep_comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sel_dep_comboActionPerformed
        ResultSet rs = null;
        ResultSet rs2 = null;
        String sql = "SELECT * FROM Subject WHERE did IN (SELECT depid from Department where depid=?) ORDER BY snumber ASC";
        String sql2 = "SELECT * FROM Teacher WHERE staffno IN (SELECT snum FROM Is_part_of where did=?) ORDER BY staffno ASC";
        classes_area.setText("");
        teachers_area.setText("");
        add_class_teacher_combo.removeAllItems();
        rem_class_combo.removeAllItems();
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, Integer.valueOf(sel_dep_combo.getSelectedItem().toString()));
            rs = pstmt.executeQuery();
            pstmt = conn.prepareStatement(sql2);
            pstmt.setInt(1, Integer.valueOf(sel_dep_combo.getSelectedItem().toString()));
            rs2 = pstmt.executeQuery();
            classes_area.append(padRight("snumber", 7) +" "+ padRight("name", 16) +" "+ 
                                padRight("year", 4) +" "+ padRight("snum", 4) +" "+ padRight("did", 6) +"\n");
            classes_area.append("-------" +" "+ "----------------" +" "+ "----" +" "+ "----" +" "+ "------\n");
            while (rs.next()) {
                classes_area.append(padRight(String.valueOf(rs.getInt("snumber")), 7) +" "+ 
                                    padRight(rs.getString("name"), 16) +" "+ 
                                    padRight(String.valueOf(rs.getInt("yr")), 4) +" "+ 
                                    padRight(String.valueOf(rs.getInt("snum")), 4) +" "+ 
                                    padRight(String.valueOf(rs.getInt("did")), 6) +"\n");
                rem_class_combo.addItem(String.valueOf(rs.getInt("snumber")));
            }
            teachers_area.append(padRight("staffno", 7) +" "+ padRight("name", 24) +" "+ 
                                 padRight("dob", 12) +" "+ padRight("addr", 24) +" "+ padRight("salary", 6) +"\n");
            teachers_area.append("-------" +" "+ 
                                 "------------------------" +" "+ 
                                 "------------" +" "+ 
                                 "------------------------" +" "+ 
                                 "------" + "\n");
            while (rs2.next()) {
                teachers_area.append (
                        padRight(String.valueOf(rs2.getInt("staffno")), 7) +" "+
                        padRight(rs2.getString("name"), 24) +" "+
                        padRight(String.valueOf(rs2.getDate("dob")), 12) +" "+
                        padRight(rs2.getString("addr"), 24) +" "+
                        padRight(String.valueOf(rs2.getInt("salary")), 6) +"\n"
                );
                add_class_teacher_combo.addItem(String.valueOf(rs2.getInt("staffno")));
            }
            rs.close();
            rs2.close();
            pstmt.close();
        } catch (NullPointerException np) {
            //do nothing
        } catch (Exception e) {
            JOptionPane.showMessageDialog(mainpane, "Error executing: " + e.toString());
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (rs2 != null) rs2.close();
                if (pstmt != null) pstmt.close();
            } catch (Exception ign) {
                //ignore
            }
        }
    }//GEN-LAST:event_sel_dep_comboActionPerformed

    private void add_class_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_add_class_btnActionPerformed
        ResultSet rs = null;
        String sql = "INSERT INTO Subject VALUES (?,?,?,?,?)";
        try {
            int snumber = Integer.valueOf(add_class_num_field.getText());
            String name = add_class_name_field.getText();
            int yr = Integer.valueOf(add_class_yr_field.getText());
            int snum = Integer.valueOf(add_class_teacher_combo.getSelectedItem().toString());
            int did = Integer.valueOf(sel_dep_combo.getSelectedItem().toString());
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, snumber);
            pstmt.setString(2, name);
            pstmt.setInt(3, yr);
            pstmt.setInt(4, snum);
            pstmt.setInt(5, did);
            rs = pstmt.executeQuery();
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(mainpane, "Error executing: " + e.toString());
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
            } catch (Exception ign) {
                //ignore
            }
        }
        int sel = sel_dep_combo.getSelectedIndex();
        mainpane.setSelectedIndex(2);
        mainpane.setSelectedIndex(3);
        sel_dep_combo.setSelectedIndex(sel);
        add_class_name_field.setText("");
        add_class_yr_field.setText("");
        add_class_num_field.setText("");
    }//GEN-LAST:event_add_class_btnActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        ResultSet rs = null;
        String sql = "DELETE FROM Subject WHERE snumber=? and did=?";
        try {
            int snumber = Integer.valueOf(rem_class_combo.getSelectedItem().toString());
            int did = Integer.valueOf(sel_dep_combo.getSelectedItem().toString());
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, snumber);
            pstmt.setInt(2, did);
            rs = pstmt.executeQuery();
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(mainpane, "Error executing: " + e.toString());
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
            } catch (Exception ign) {
                //ignore
            }
        }
        int sel = sel_dep_combo.getSelectedIndex();
        mainpane.setSelectedIndex(2);
        mainpane.setSelectedIndex(3);
        sel_dep_combo.setSelectedIndex(sel);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void aboutbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutbtnActionPerformed
        JOptionPane.showMessageDialog(mainpane, "SchoolDB\n"
                + "Author: Thanasi Poulos\n"
                + "For: 3623ICT Database Systems and Administration\n"
                + "This Build: v1.0 (03/06/2015)");
    }//GEN-LAST:event_aboutbtnActionPerformed

    private void class_seldep_comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_class_seldep_comboActionPerformed
        ResultSet rs = null;
        String sql = "SELECT snumber FROM Subject where did=?";
        class_selclass_combo.removeAllItems();
        try {
            int did = Integer.valueOf(class_seldep_combo.getSelectedItem().toString());
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, did);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                class_selclass_combo.addItem(String.valueOf(rs.getInt("snumber")));
            }
            rs.close();
            pstmt.close();
        } catch (NullPointerException np) {
            System.out.println("NullPointer");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(mainpane, "Error executing: " + e.toString());
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
            } catch (Exception ign) {
                //ignore
            }
        }
    }//GEN-LAST:event_class_seldep_comboActionPerformed

    private void class_selclass_comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_class_selclass_comboActionPerformed
        ResultSet rs = null;
        ResultSet rs2 = null;
        String sql = "SELECT * FROM Student s where s.snumber IN "
                + "(SELECT a.studentnum FROM Attends a WHERE a.subjectnum=? "
                + "AND a.did=?)";
        String sql2 = "SELECT s.name, a.grade FROM Student s, Attends a WHERE "
                + "s.snumber IN (SELECT studentnum FROM Attends WHERE "
                + "a.studentnum = studentnum AND a.subjectnum=? AND a.did=?)";
        class_student_area.setText("");
        class_grades_area.setText("");
        try {
            int subjectnum = Integer.valueOf(class_selclass_combo.getSelectedItem().toString());
            int did = Integer.valueOf(class_seldep_combo.getSelectedItem().toString());
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, subjectnum);
            pstmt.setInt(2, did);
            rs = pstmt.executeQuery();
            class_student_area.append("snumber " + padRight("name", 24) +" "+ padRight("dob", 12) +" "+
                    padRight("addr", 24) +"\n");
            class_student_area.append("------- " + "------------------------ " 
                    + "------------ " + "------------------------\n");
            while(rs.next()) {
                class_student_area.append(
                        padRight(String.valueOf(rs.getInt("snumber")),7) +" "+ 
                        padRight(rs.getString("name"),24) +" "+ 
                        padRight(String.valueOf(rs.getDate("dob")),12) +" "+ 
                        padRight(rs.getString("addr"),24) +"\n"
                );
            }
            pstmt = conn.prepareStatement(sql2);
            pstmt.setInt(1, subjectnum);
            pstmt.setInt(2, did);
            rs2 = pstmt.executeQuery();
            while (rs2.next()) {
                class_grades_area.append(padRight(rs2.getString("name"),24)+" "+
                                         String.valueOf(rs2.getInt("grade")) + "\n");
            }
            rs2.close();
            rs.close();
            pstmt.close();
        } catch (NullPointerException np) {
            //ignore
        } catch (Exception e) {
            JOptionPane.showMessageDialog(mainpane, "Error executing: " + e.toString());
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (rs2 != null) rs2.close();
                if (pstmt != null) pstmt.close();
            } catch (Exception ign) {
                //ignore
            }
        }
    }//GEN-LAST:event_class_selclass_comboActionPerformed

    private void fgrade_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fgrade_btnActionPerformed
        ResultSet rs = null;
        String sql = "SELECT s.name, s.snumber FROM Student s WHERE s.snumber IN "
                + "(SELECT a.studentnum FROM Attends a "
                + "GROUP BY a.studentnum HAVING AVG(a.grade) > ?)";
        fgrade_area.setText("");
        try {
            float grade = Float.valueOf(fgrade_field.getText());
            pstmt = conn.prepareStatement(sql);
            pstmt.setFloat(1, grade);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                fgrade_area.append(padRight(rs.getString("name"),24)+" "+
                        padRight(String.valueOf(rs.getInt("snumber")),6)+"\n");
            }
            rs.close();
            pstmt.close();
        } catch (NullPointerException np) {
            System.out.println("NullPointer");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(mainpane, "Error executing: " + e.toString());
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
            } catch (Exception ign) {
                //ignore
            }
        }
    }//GEN-LAST:event_fgrade_btnActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
            /*UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            UIManager.LookAndFeelInfo[] lafi = UIManager.getInstalledLookAndFeels();
            for (int i = 0; i < lafi.length; i++) {
                System.out.println(lafi[i].getName());
            }*/
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        try {
            if (args[0].equals("-def")) {
                defaultValues = true;
            } else if (args[0].equals("-uni")) {
                uniValues = true;
            }
        } catch (Exception e) {
            
        }
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton aboutbtn;
    private javax.swing.JButton add_class_btn;
    private javax.swing.JTextField add_class_name_field;
    private javax.swing.JTextField add_class_num_field;
    private javax.swing.JComboBox<String> add_class_teacher_combo;
    private javax.swing.JTextField add_class_yr_field;
    private javax.swing.JButton add_dep_btn;
    private javax.swing.JTextField add_dep_budget_field;
    private javax.swing.JTextField add_dep_code_field;
    private javax.swing.JTextField add_dep_id_field;
    private javax.swing.JTextField add_dep_name_field;
    private javax.swing.JComboBox<String> add_dep_school_combo;
    private javax.swing.JButton addpcode_btn;
    private javax.swing.JComboBox<String> addpcode_combo;
    private javax.swing.JTextField addpcode_field;
    private javax.swing.JButton addusrbtn;
    private javax.swing.JTextField addusrfield;
    private javax.swing.JComboBox addusrpermcombo;
    private javax.swing.JPasswordField addusrpwfield;
    private javax.swing.JComboBox<String> addusrschoolcombo;
    private javax.swing.JTextArea admin_textarea;
    private javax.swing.JTextField adminfield;
    private javax.swing.JButton alter_dep_btn;
    private javax.swing.JComboBox alter_dep_combo;
    private javax.swing.JTextField alter_dep_field;
    private javax.swing.JComboBox<String> alter_dep_id_combo;
    private javax.swing.JTextArea class_grades_area;
    private javax.swing.JComboBox<String> class_selclass_combo;
    private javax.swing.JComboBox<String> class_seldep_combo;
    private javax.swing.JTextArea class_student_area;
    private javax.swing.JTextArea classes_area;
    private javax.swing.JTextArea deps_area;
    private javax.swing.JButton dist_refresh;
    private javax.swing.JTextArea dist_textarea;
    private javax.swing.JComboBox<String> district_combo;
    private javax.swing.JTextArea fgrade_area;
    private javax.swing.JButton fgrade_btn;
    private javax.swing.JTextField fgrade_field;
    private javax.swing.JLabel infolabel;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JButton loginbutton;
    private javax.swing.JPanel loginpanel;
    private javax.swing.JTabbedPane mainpane;
    private javax.swing.JComboBox<String> postcodes_combo;
    private javax.swing.JTextArea postcodestextarea;
    private javax.swing.JPasswordField pwfield;
    private javax.swing.JLabel pwlabel;
    private javax.swing.JButton refreshdeps_btn;
    private javax.swing.JComboBox<String> rem_class_combo;
    private javax.swing.JButton rem_dep_btn;
    private javax.swing.JComboBox<String> rem_dep_combo;
    private javax.swing.JButton rempcode_btn;
    private javax.swing.JComboBox<String> rempcode_dnum_combo;
    private javax.swing.JComboBox<String> rempcode_pcode_combo;
    private javax.swing.JButton rename_btn;
    private javax.swing.JComboBox<String> rename_combo;
    private javax.swing.JTextField rename_field;
    private javax.swing.JButton resubinfo;
    private javax.swing.JTextArea schools_textarea;
    private javax.swing.JComboBox<String> sel_dep_combo;
    private javax.swing.JTextArea teachers_area;
    private javax.swing.JTextField unfield;
    private javax.swing.JLabel unlabel;
    // End of variables declaration//GEN-END:variables
}
