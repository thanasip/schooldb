/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3612ict;

/**
 *
 * @author Thanasi
 */
public class User {
    public String uname = "";
    public String perms = "";
    public String school = "";
    
    public User() {
        
    }
    
    @Override
    public String toString() {
        return ("Username: " + uname + ", Permissions: " + perms + ", School: " + school);
    }
}
