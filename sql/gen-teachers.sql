DECLARE
    num number(2);
    fname varchar(16);
    lname varchar(16);
    addr varchar(64);
    type fnamearray IS VARRAY(10) OF VARCHAR(16);
    type lnamearray IS VARRAY(10) OF VARCHAR(16);
    type addrarray IS VARRAY(10) OF VARCHAR(32);
    fnames fnamearray;
    lnames lnamearray;
    addrs addrarray;
BEGIN
    fnames := fnamearray('Robert ', 'James ', 'Samuel ', 'William ', 'Arthur ', 'Sarah ', 'Aurora ', 'Christine ', 'Rachael ', 'Hayley ');
    lnames := lnamearray('Williams', 'Croydon', 'Samuelson', 'Ford', 'Smith', 'Black', 'Green' , 'White', 'Jimson', 'Vines');
    addrs := addrarray(' Trish St', ' Warble Av', ' Right Wy', ' Rupari Rd', ' Castle Pde', ' Westminster Dr', ' Spring St', ' First Av', ' Cantebury Dr', ' Park Rd West');

    FOR i IN 1..500 LOOP
        SELECT ROUND(DBMS_RANDOM.VALUE(1, 10)) INTO num FROM dual;
        fname := fnames(num);

        SELECT ROUND(DBMS_RANDOM.VALUE(1, 10)) INTO num FROM dual;
        lname := lnames(num);

        SELECT ROUND(DBMS_RANDOM.VALUE(1, 10)) INTO num FROM dual;
        addr := CONCAT(ROUND(DBMS_RANDOM.VALUE(1, 999)), addrs(num));

        INSERT INTO TEACHER VALUES (LPAD(ROUND(i), 5,'0'), CONCAT(fname, lname), TRUNC(SYSDATE-25000 + DBMS_RANDOM.value(0, 16000)), addr, ROUND(DBMS_RANDOM.VALUE(40000, 50000)));
    END LOOP;
END;