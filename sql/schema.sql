CREATE TABLE District (
    dnumber number(5) NOT NULL,
    dname varchar(32) NOT NULL,
    PRIMARY KEY (dnumber),
    UNIQUE (dname)
);

CREATE TABLE Postcodes (
    pcode number(4) NOT NULL,
    dnum number(5) NOT NULL,
    PRIMARY KEY (pcode, dnum)
);

ALTER TABLE Postcodes ADD CONSTRAINT pcodes_dnum
FOREIGN KEY(dnum) REFERENCES District(dnumber);

CREATE TABLE School (
    sname varchar(32) NOT NULL,
    type varchar(8) NOT NULL,
    addr varchar(64) NOT NULL,
    dnum number(5) NOT NULL,
    PRIMARY KEY (sname),
    CHECK (type = 'Primary' OR type = 'High')
);

ALTER TABLE School ADD CONSTRAINT school_dnum
FOREIGN KEY(dnum) REFERENCES District(dnumber);

CREATE TABLE Department (
    depid number(5) NOT NULL,
    code varchar(3) NOT NULL,
    name varchar(16) NOT NULL,
    budget number(5) NOT NULL,
    schoolname varchar(32) NOT NULL,
    PRIMARY KEY (depid),
    CHECK (budget >= 1000 AND budget <= 10000)
);

ALTER TABLE Department ADD CONSTRAINT dept_sname
FOREIGN KEY(schoolname) REFERENCES School(sname);

CREATE TABLE Student (
    snumber number(5) NOT NULL,
    name varchar(32) NOT NULL,
    dob date NOT NULL,
    addr varchar(64) NOT NULL,
    schoolname varchar(32) NOT NULL,
    PRIMARY KEY (snumber)
);

ALTER TABLE Student ADD CONSTRAINT student_sname
FOREIGN KEY (schoolname) REFERENCES School(sname);

CREATE TABLE Phonenumbers (
    pnumber number(10) NOT NULL,
    snum number(5) NOT NULL,
    PRIMARY KEY (pnumber, snum)
);

ALTER TABLE Phonenumbers ADD CONSTRAINT pn_snum
FOREIGN KEY(snum) REFERENCES Student(snumber);

CREATE TABLE Teacher (
    staffno number(5) NOT NULL,
    name varchar(32) NOT NULL,
    dob date NOT NULL,
    addr varchar(64) NOT NULL,
    salary number(6) NOT NULL,
    PRIMARY KEY (staffno)
);

CREATE TABLE Is_part_of (
    snum number(5) NOT NULL,
    did number(5) NOT NULL,
    PRIMARY KEY (snum, did)
);

ALTER TABLE Is_part_of ADD CONSTRAINT ipf_snum
FOREIGN KEY(snum) REFERENCES Teacher(staffno);

ALTER TABLE Is_part_of ADD CONSTRAINT ipf_dcode
FOREIGN KEY(did) REFERENCES Department(depid);

CREATE TABLE Subject (
    snumber number(3) NOT NULL,
    name varchar(16) NOT NULL,
    yr number(2) NOT NULL,
    snum number(5) NOT NULL,
    did number(5) NOT NULL,
    PRIMARY KEY (snumber, did)
);

ALTER TABLE Subject ADD CONSTRAINT subj_dcode
FOREIGN KEY(did) REFERENCES Department(depid);

ALTER TABLE Subject ADD CONSTRAINT subj_snum
FOREIGN KEY(snum) REFERENCES Teacher(staffno);

CREATE TABLE Attends (
    studentnum number(5) NOT NULL,
    subjectnum number(3) NOT NULL,
    did number(5) NOT NULL,
    grade number(1) NOT NULL,
    PRIMARY KEY (subjectnum, studentnum, did),
    CHECK (grade >= 1 AND grade <= 7)
);

ALTER TABLE Attends ADD CONSTRAINT att_snum
FOREIGN KEY(studentnum) REFERENCES Student(snumber);

ALTER TABLE Attends ADD CONSTRAINT att_num_did
FOREIGN KEY(did, subjectnum) REFERENCES Subject(did, snumber);

CREATE TABLE Resourcelist (
    isbn number(13) NOT NULL,
    title varchar(32) NOT NULL,
    author varchar(64) NOT NULL,
    ordnum number(5) NOT NULL,
    PRIMARY KEY (isbn)
);

CREATE TABLE Orders (
    risbn number(5) NOT NULL,
    did number(5) NOT NULL,
    cost number(5) NOT NULL,
    odate date NOT NULL,
    PRIMARY KEY (risbn, did)
);

ALTER TABLE Orders ADD CONSTRAINT ord_dcode
FOREIGN KEY(did) REFERENCES Department(depid);

ALTER TABLE Orders ADD CONSTRAINT ord_isbn
FOREIGN KEY(risbn) REFERENCES Resourcelist(isbn);

CREATE TABLE Textbook (
    risbn number(13) NOT NULL,
    topic varchar(16) NOT NULL,
    ed number(2),
    PRIMARY KEY (risbn)
);

ALTER TABLE Textbook ADD CONSTRAINT txb_isbn
FOREIGN KEY(risbn) REFERENCES Resourcelist(isbn);

CREATE TABLE Onlineres (
    risbn number(13) NOT NULL,
    genre varchar(16) NOT NULL,
    url varchar(128) NOT NULL,
    PRIMARY KEY (risbn)
);

ALTER TABLE Onlineres ADD CONSTRAINT onl_isbn
FOREIGN KEY(risbn) REFERENCES Resourcelist(isbn);

CREATE TABLE Users (
    uname varchar(16) NOT NULL,
    passw varchar(32) NOT NULL,
    perms varchar(10) NOT NULL,
    school varchar(32),
    UNIQUE(uname)
);

ALTER TABLE Users ADD CONSTRAINT usr_school
FOREIGN KEY(school) REFERENCES School(sname);