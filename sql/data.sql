-- Districts
INSERT INTO DISTRICT (dnumber, dname) VALUES (1, 'Northside');
INSERT INTO DISTRICT (dnumber, dname) VALUES (2, 'Southside');
INSERT INTO DISTRICT (dnumber, dname) VALUES (3, 'Eastside');
INSERT INTO DISTRICT (dnumber, dname) VALUES (4, 'Westside');
INSERT INTO DISTRICT (dnumber, dname) VALUES (5, 'Regional');

-- Postcodes
INSERT INTO POSTCODES (pcode, dnum) VALUES (1000, 1);
INSERT INTO POSTCODES (pcode, dnum) VALUES (1001, 1);
INSERT INTO POSTCODES (pcode, dnum) VALUES (1002, 1);
INSERT INTO POSTCODES (pcode, dnum) VALUES (1003, 1);
INSERT INTO POSTCODES (pcode, dnum) VALUES (1004, 1);
INSERT INTO POSTCODES (pcode, dnum) VALUES (2000, 2);
INSERT INTO POSTCODES (pcode, dnum) VALUES (2001, 2);
INSERT INTO POSTCODES (pcode, dnum) VALUES (2002, 2);
INSERT INTO POSTCODES (pcode, dnum) VALUES (2003, 2);
INSERT INTO POSTCODES (pcode, dnum) VALUES (2004, 2);
INSERT INTO POSTCODES (pcode, dnum) VALUES (3000, 3);
INSERT INTO POSTCODES (pcode, dnum) VALUES (3001, 3);
INSERT INTO POSTCODES (pcode, dnum) VALUES (3002, 3);
INSERT INTO POSTCODES (pcode, dnum) VALUES (3003, 3);
INSERT INTO POSTCODES (pcode, dnum) VALUES (3004, 3);
INSERT INTO POSTCODES (pcode, dnum) VALUES (4000, 4);
INSERT INTO POSTCODES (pcode, dnum) VALUES (4001, 4);
INSERT INTO POSTCODES (pcode, dnum) VALUES (4002, 4);
INSERT INTO POSTCODES (pcode, dnum) VALUES (4003, 4);
INSERT INTO POSTCODES (pcode, dnum) VALUES (4004, 4);
INSERT INTO POSTCODES (pcode, dnum) VALUES (5000, 5);
INSERT INTO POSTCODES (pcode, dnum) VALUES (5001, 5);
INSERT INTO POSTCODES (pcode, dnum) VALUES (5002, 5);
INSERT INTO POSTCODES (pcode, dnum) VALUES (5003, 5);
INSERT INTO POSTCODES (pcode, dnum) VALUES (5004, 5);

-- Schools
INSERT INTO SCHOOL (sname, type, addr, dnum) VALUES ('School A', 'Primary', '1 Fakes St 1001', 1);
INSERT INTO SCHOOL (sname, type, addr, dnum) VALUES ('School B', 'Primary', '2 Plums St 2001', 2);
INSERT INTO SCHOOL (sname, type, addr, dnum) VALUES ('School C', 'Primary', '3 Quack St 3001', 3);
INSERT INTO SCHOOL (sname, type, addr, dnum) VALUES ('School D', 'Primary', '4 Groff St 4001', 4);
INSERT INTO SCHOOL (sname, type, addr, dnum) VALUES ('School E', 'Primary', '5 Sobes St 5001', 5);

-- Departments
INSERT INTO DEPARTMENT (depid, code, name, budget, schoolname) VALUES (11111, 'SCI', 'Science', 5000, 'School A');
INSERT INTO DEPARTMENT (depid, code, name, budget, schoolname) VALUES (11112, 'MAT', 'Maths', 4000, 'School A');
INSERT INTO DEPARTMENT (depid, code, name, budget, schoolname) VALUES (11113, 'ENG', 'English', 6000, 'School A');

INSERT INTO DEPARTMENT (depid, code, name, budget, schoolname) VALUES (21111, 'SCI', 'Science', 3500, 'School B');
INSERT INTO DEPARTMENT (depid, code, name, budget, schoolname) VALUES (21112, 'MAT', 'Maths', 7650, 'School B');
INSERT INTO DEPARTMENT (depid, code, name, budget, schoolname) VALUES (21113, 'ENG', 'English', 5450, 'School B');

INSERT INTO DEPARTMENT (depid, code, name, budget, schoolname) VALUES (31111, 'SCI', 'Science', 9800, 'School C');
INSERT INTO DEPARTMENT (depid, code, name, budget, schoolname) VALUES (31112, 'MAT', 'Maths', 5670, 'School C');
INSERT INTO DEPARTMENT (depid, code, name, budget, schoolname) VALUES (31113, 'ENG', 'English', 2340, 'School C');

INSERT INTO DEPARTMENT (depid, code, name, budget, schoolname) VALUES (41111, 'SCI', 'Science', 6470, 'School D');
INSERT INTO DEPARTMENT (depid, code, name, budget, schoolname) VALUES (41112, 'MAT', 'Maths', 3485, 'School D');
INSERT INTO DEPARTMENT (depid, code, name, budget, schoolname) VALUES (41113, 'ENG', 'English', 2320, 'School D');

INSERT INTO DEPARTMENT (depid, code, name, budget, schoolname) VALUES (51111, 'SCI', 'Science', 6560, 'School E');
INSERT INTO DEPARTMENT (depid, code, name, budget, schoolname) VALUES (51112, 'MAT', 'Maths', 8870, 'School E');
INSERT INTO DEPARTMENT (depid, code, name, budget, schoolname) VALUES (51113, 'ENG', 'English', 7780, 'School E');

-- Students
DECLARE
    num number(2);
    sch varchar(32);
    fname varchar(16);
    lname varchar(16);
    addr varchar(64);
    type fnamearray IS VARRAY(10) OF VARCHAR(16);
    type lnamearray IS VARRAY(10) OF VARCHAR(16);
    type addrarray IS VARRAY(10) OF VARCHAR(32);
    fnames fnamearray;
    lnames lnamearray;
    addrs addrarray;
BEGIN
    fnames := fnamearray('Robert ', 'James ', 'Samuel ', 'William ', 'Arthur ', 'Sarah ', 'Aurora ', 'Christine ', 'Rachael ', 'Hayley ');
    lnames := lnamearray('Williams', 'Croydon', 'Samuelson', 'Ford', 'Smith', 'Black', 'Green' , 'White', 'Jimson', 'Vines');
    addrs := addrarray(' Trish St', ' Warble Av', ' Right Wy', ' Rupari Rd', ' Castle Pde', ' Westminster Dr', ' Spring St', ' First Av', ' Cantebury Dr', ' Park Rd West');

    FOR i IN 1..10000 LOOP
        SELECT ROUND(DBMS_RANDOM.VALUE(1, 5)) INTO num FROM dual;
        IF num = 1 
            THEN sch := 'School A';
        ELSIF num = 2 
            THEN sch := 'School B';
        ELSIF num = 3 
            THEN sch := 'School C';
        ELSIF num = 4 
            THEN sch := 'School D';
        ELSE sch := 'School E';
        END IF; 

        SELECT ROUND(DBMS_RANDOM.VALUE(1, 10)) INTO num FROM dual;
        fname := fnames(num);

        SELECT ROUND(DBMS_RANDOM.VALUE(1, 10)) INTO num FROM dual;
        lname := lnames(num);

        SELECT ROUND(DBMS_RANDOM.VALUE(1, 10)) INTO num FROM dual;
        addr := CONCAT(ROUND(DBMS_RANDOM.VALUE(1, 999)), addrs(num));

        INSERT INTO STUDENT VALUES (LPAD(ROUND(i), 5,'0'), CONCAT(fname, lname), TRUNC(SYSDATE-8395 + DBMS_RANDOM.value(0, 6570)), addr, sch);
    END LOOP;
END;
/

-- Student Phone Numbers
BEGIN
    FOR i IN 1..10000 LOOP
        INSERT INTO PHONENUMBERS VALUES (ROUND(DBMS_RANDOM.VALUE(30000000, 39999999)), i);
        INSERT INTO PHONENUMBERS VALUES (ROUND(DBMS_RANDOM.VALUE(30000000, 39999999)), i);
    END LOOP;
END;
/

-- Teachers
DECLARE
    num number(2);
    fname varchar(16);
    lname varchar(16);
    addr varchar(64);
    type fnamearray IS VARRAY(10) OF VARCHAR(16);
    type lnamearray IS VARRAY(10) OF VARCHAR(16);
    type addrarray IS VARRAY(10) OF VARCHAR(32);
    fnames fnamearray;
    lnames lnamearray;
    addrs addrarray;
BEGIN
    fnames := fnamearray('Robert ', 'James ', 'Samuel ', 'William ', 'Arthur ', 'Sarah ', 'Aurora ', 'Christine ', 'Rachael ', 'Hayley ');
    lnames := lnamearray('Williams', 'Croydon', 'Samuelson', 'Ford', 'Smith', 'Black', 'Green' , 'White', 'Jimson', 'Vines');
    addrs := addrarray(' Trish St', ' Warble Av', ' Right Wy', ' Rupari Rd', ' Castle Pde', ' Westminster Dr', ' Spring St', ' First Av', ' Cantebury Dr', ' Park Rd West');

    FOR i IN 1..500 LOOP
        SELECT ROUND(DBMS_RANDOM.VALUE(1, 10)) INTO num FROM dual;
        fname := fnames(num);

        SELECT ROUND(DBMS_RANDOM.VALUE(1, 10)) INTO num FROM dual;
        lname := lnames(num);

        SELECT ROUND(DBMS_RANDOM.VALUE(1, 10)) INTO num FROM dual;
        addr := CONCAT(ROUND(DBMS_RANDOM.VALUE(1, 999)), addrs(num));

        INSERT INTO TEACHER VALUES (LPAD(ROUND(i), 5,'0'), CONCAT(fname, lname), TRUNC(SYSDATE-25000 + DBMS_RANDOM.value(0, 16000)), addr, ROUND(DBMS_RANDOM.VALUE(40000, 50000)));
    END LOOP;
END;
/

-- Teacher Departments
DECLARE
    num number(2);
BEGIN
    FOR i IN 1..500 LOOP
        SELECT ROUND(DBMS_RANDOM.VALUE(1, 15)) INTO num FROM dual;
        IF num = 1
            THEN 
            INSERT INTO IS_PART_OF VALUES (i, 11111);
            INSERT INTO IS_PART_OF VALUES (i, 11113);
        ELSIF num = 2
            THEN
            INSERT INTO IS_PART_OF VALUES (i, 11112);
            INSERT INTO IS_PART_OF VALUES (i, 11111);
        ELSIF num = 3
            THEN
            INSERT INTO IS_PART_OF VALUES (i, 11113);
            INSERT INTO IS_PART_OF VALUES (i, 11112);
        ELSIF num = 4
            THEN 
            INSERT INTO IS_PART_OF VALUES (i, 21111);
            INSERT INTO IS_PART_OF VALUES (i, 21113);
        ELSIF num = 5
            THEN 
            INSERT INTO IS_PART_OF VALUES (i, 21112);
            INSERT INTO IS_PART_OF VALUES (i, 21111);
        ELSIF num = 6
            THEN 
            INSERT INTO IS_PART_OF VALUES (i, 21113);
            INSERT INTO IS_PART_OF VALUES (i, 21112);
        ELSIF num = 7
            THEN 
            INSERT INTO IS_PART_OF VALUES (i, 31111);
            INSERT INTO IS_PART_OF VALUES (i, 31113);
        ELSIF num = 8
            THEN 
            INSERT INTO IS_PART_OF VALUES (i, 31112);
            INSERT INTO IS_PART_OF VALUES (i, 31111);
        ELSIF num = 9
            THEN 
            INSERT INTO IS_PART_OF VALUES (i, 31113);
            INSERT INTO IS_PART_OF VALUES (i, 31112);
        ELSIF num = 10
            THEN 
            INSERT INTO IS_PART_OF VALUES (i, 41111);
            INSERT INTO IS_PART_OF VALUES (i, 41113);
        ELSIF num = 11
            THEN 
            INSERT INTO IS_PART_OF VALUES (i, 41112);
            INSERT INTO IS_PART_OF VALUES (i, 41111);
        ELSIF num = 12
            THEN 
            INSERT INTO IS_PART_OF VALUES (i, 41113);
            INSERT INTO IS_PART_OF VALUES (i, 41112);
        ELSIF num = 13
            THEN 
            INSERT INTO IS_PART_OF VALUES (i, 51111);
            INSERT INTO IS_PART_OF VALUES (i, 51113);
        ELSIF num = 14
            THEN 
            INSERT INTO IS_PART_OF VALUES (i, 51112);
            INSERT INTO IS_PART_OF VALUES (i, 51111);
        ELSIF num = 15
            THEN 
            INSERT INTO IS_PART_OF VALUES (i, 51113);
            INSERT INTO IS_PART_OF VALUES (i, 51112);
        END IF;
    END LOOP;
END;
/

-- Subjects

-- School A
INSERT INTO Subject VALUES (101, 'Science A', '15', 1, 11111);
INSERT INTO Subject VALUES (102, 'Science B', '15', 2, 11111);
INSERT INTO Subject VALUES (103, 'Science C', '15', 3, 11111);

INSERT INTO Subject VALUES (201, 'Maths A', '15', 4, 11112);
INSERT INTO Subject VALUES (202, 'Maths B', '15', 5, 11112);
INSERT INTO Subject VALUES (203, 'Maths C', '15', 6, 11112);

INSERT INTO Subject VALUES (301, 'English A', '15', 7, 11113);
INSERT INTO Subject VALUES (302, 'English B', '15', 8, 11113);
INSERT INTO Subject VALUES (303, 'English C', '15', 9, 11113);

-- School B
INSERT INTO Subject VALUES (401, 'Science A', '15', 10, 21111);
INSERT INTO Subject VALUES (402, 'Science B', '15', 20, 21111);
INSERT INTO Subject VALUES (403, 'Science C', '15', 30, 21111);

INSERT INTO Subject VALUES (501, 'Maths A', '15', 40, 21112);
INSERT INTO Subject VALUES (502, 'Maths B', '15', 50, 21112);
INSERT INTO Subject VALUES (503, 'Maths C', '15', 60, 21112);

INSERT INTO Subject VALUES (601, 'English A', '15', 70, 21113);
INSERT INTO Subject VALUES (602, 'English B', '15', 80, 21113);
INSERT INTO Subject VALUES (603, 'English C', '15', 90, 21113);

-- School C
INSERT INTO Subject VALUES (701, 'Science A', '15', 11, 31111);
INSERT INTO Subject VALUES (702, 'Science B', '15', 21, 31111);
INSERT INTO Subject VALUES (703, 'Science C', '15', 31, 31111);

INSERT INTO Subject VALUES (801, 'Maths A', '15', 41, 31112);
INSERT INTO Subject VALUES (802, 'Maths B', '15', 51, 31112);
INSERT INTO Subject VALUES (803, 'Maths C', '15', 61, 31112);

INSERT INTO Subject VALUES (901, 'English A', '15', 71, 31113);
INSERT INTO Subject VALUES (902, 'English B', '15', 81, 31113);
INSERT INTO Subject VALUES (903, 'English C', '15', 91, 31113);

-- School D
INSERT INTO Subject VALUES (107, 'Science A', '15', 21, 41111);
INSERT INTO Subject VALUES (108, 'Science B', '15', 22, 41111);
INSERT INTO Subject VALUES (109, 'Science C', '15', 23, 41111);

INSERT INTO Subject VALUES (207, 'Maths A', '15', 24, 41112);
INSERT INTO Subject VALUES (208, 'Maths B', '15', 25, 41112);
INSERT INTO Subject VALUES (209, 'Maths C', '15', 26, 41112);

INSERT INTO Subject VALUES (307, 'English A', '15', 27, 41113);
INSERT INTO Subject VALUES (308, 'English B', '15', 28, 41113);
INSERT INTO Subject VALUES (309, 'English C', '15', 29, 41113);

-- School E
INSERT INTO Subject VALUES (407, 'Science A', '15', 17, 51111);
INSERT INTO Subject VALUES (408, 'Science B', '15', 27, 51111);
INSERT INTO Subject VALUES (409, 'Science C', '15', 37, 51111);

INSERT INTO Subject VALUES (507, 'Maths A', '15', 47, 51112);
INSERT INTO Subject VALUES (508, 'Maths B', '15', 57, 51112);
INSERT INTO Subject VALUES (509, 'Maths C', '15', 67, 51112);

INSERT INTO Subject VALUES (607, 'English A', '15', 77, 51113);
INSERT INTO Subject VALUES (608, 'English B', '15', 87, 51113);
INSERT INTO Subject VALUES (609, 'English C', '15', 97, 51113);

-- Attendance
BEGIN
    -- School A
    FOR i IN 1..30 LOOP
        INSERT INTO Attends VALUES (i, 101, 11111, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 201, 11112, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 301, 11113, ROUND(DBMS_RANDOM.VALUE(1, 7)));
    END LOOP;

    FOR i IN 31..60 LOOP
        INSERT INTO Attends VALUES (i, 102, 11111, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 202, 11112, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 302, 11113, ROUND(DBMS_RANDOM.VALUE(1, 7)));
    END LOOP;

    FOR i IN 61..90 LOOP
        INSERT INTO Attends VALUES (i, 103, 11111, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 203, 11112, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 303, 11113, ROUND(DBMS_RANDOM.VALUE(1, 7)));
    END LOOP;

    -- School B
    FOR i IN 91..120 LOOP
        INSERT INTO Attends VALUES (i, 401, 21111, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 501, 21112, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 601, 21113, ROUND(DBMS_RANDOM.VALUE(1, 7)));
    END LOOP;

    FOR i IN 121..150 LOOP
        INSERT INTO Attends VALUES (i, 402, 21111, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 502, 21112, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 602, 21113, ROUND(DBMS_RANDOM.VALUE(1, 7)));
    END LOOP;

    FOR i IN 151..180 LOOP
        INSERT INTO Attends VALUES (i, 403, 21111, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 503, 21112, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 603, 21113, ROUND(DBMS_RANDOM.VALUE(1, 7)));
    END LOOP;

    -- School C
    FOR i IN 181..210 LOOP
        INSERT INTO Attends VALUES (i, 701, 31111, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 801, 31112, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 901, 31113, ROUND(DBMS_RANDOM.VALUE(1, 7)));
    END LOOP;

    FOR i IN 211..240 LOOP
        INSERT INTO Attends VALUES (i, 702, 31111, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 802, 31112, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 902, 31113, ROUND(DBMS_RANDOM.VALUE(1, 7)));
    END LOOP;

    FOR i IN 241..270 LOOP
        INSERT INTO Attends VALUES (i, 703, 31111, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 803, 31112, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 903, 31113, ROUND(DBMS_RANDOM.VALUE(1, 7)));
    END LOOP;

    -- School D
    FOR i IN 271..300 LOOP
        INSERT INTO Attends VALUES (i, 107, 41111, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 207, 41112, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 307, 41113, ROUND(DBMS_RANDOM.VALUE(1, 7)));
    END LOOP;

    FOR i IN 301..330 LOOP
        INSERT INTO Attends VALUES (i, 108, 41111, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 208, 41112, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 308, 41113, ROUND(DBMS_RANDOM.VALUE(1, 7)));
    END LOOP;

    FOR i IN 331..360 LOOP
        INSERT INTO Attends VALUES (i, 109, 41111, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 209, 41112, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 309, 41113, ROUND(DBMS_RANDOM.VALUE(1, 7)));
    END LOOP;

    -- School E
    FOR i IN 361..390 LOOP
        INSERT INTO Attends VALUES (i, 407, 51111, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 507, 51112, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 607, 51113, ROUND(DBMS_RANDOM.VALUE(1, 7)));
    END LOOP;

    FOR i IN 391..420 LOOP
        INSERT INTO Attends VALUES (i, 408, 51111, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 508, 51112, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 608, 51113, ROUND(DBMS_RANDOM.VALUE(1, 7)));
    END LOOP;

    FOR i IN 421..450 LOOP
        INSERT INTO Attends VALUES (i, 409, 51111, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 509, 51112, ROUND(DBMS_RANDOM.VALUE(1, 7)));
        INSERT INTO Attends VALUES (i, 609, 51113, ROUND(DBMS_RANDOM.VALUE(1, 7)));
    END LOOP;
END;
/

-- Texbooks
INSERT INTO Resourcelist VALUES ('100', 'Resource 1', 'John Snow', 1);
INSERT INTO Resourcelist VALUES ('200', 'Resource 2', 'Jane Green', 1);
INSERT INTO Resourcelist VALUES ('300', 'Resource 3', 'Jack White', 1);
INSERT INTO Resourcelist VALUES ('400', 'Resource 4', 'Sam Prost', 1);
INSERT INTO Resourcelist VALUES ('500', 'Resource 5', 'Foss Derbin', 1);
--
INSERT INTO Textbook VALUES ('100', 'Maths A', 2);
INSERT INTO Textbook VALUES ('200', 'Maths B', 4);
INSERT INTO Textbook VALUES ('300', 'Science A', 2);
INSERT INTO Textbook VALUES ('400', 'Science B', 3);
INSERT INTO Textbook VALUES ('500', 'English', 7);

-- Online
INSERT INTO Resourcelist VALUES ('600', 'Resource 6', 'Solid Snake', 1);
INSERT INTO Resourcelist VALUES ('700', 'Resource 7', 'Liquid Ocelot', 1);
INSERT INTO Resourcelist VALUES ('800', 'Resource 8', 'Sunny Gurlukovich', 1);
INSERT INTO Resourcelist VALUES ('900', 'Resource 9', 'James Jamieson', 1);
INSERT INTO Resourcelist VALUES ('1000', 'Resource 0', 'Aaron Aaronsberg', 1);
--
INSERT INTO Onlineres VALUES ('600', 'Maths C', 'www.maths-c.com');
INSERT INTO Onlineres VALUES ('700', 'Science C', 'www.science-c.com');
INSERT INTO Onlineres VALUES ('800', 'Advanced Science', 'www.advsci.com');
INSERT INTO Onlineres VALUES ('900', 'Advanced Maths', 'www.advmath.com');
INSERT INTO Onlineres VALUES ('1000', 'English Comm', 'www.engcomm.com');

-- Orders FINALLY THE LAST SET OF VALUES UGH
INSERT INTO Orders VALUES (100, 11112, 3000, '10/JAN/10');
INSERT INTO Orders VALUES (200, 11112, 4500, '10/JAN/10');
INSERT INTO Orders VALUES (600, 11112, 9000, '10/JAN/10');

INSERT INTO Orders VALUES (300, 11111, 4580, '10/JAN/10');
INSERT INTO Orders VALUES (400, 11111, 7895, '10/JAN/10');
INSERT INTO Orders VALUES (800, 11111, 7475, '10/JAN/10');

INSERT INTO Orders VALUES (500, 11113, 3505, '10/JAN/10');
INSERT INTO Orders VALUES (1000, 11113, 7850, '10/JAN/10');

--

INSERT INTO Orders VALUES (100, 21112, 3000, '10/JAN/10');
INSERT INTO Orders VALUES (200, 21112, 4500, '10/JAN/10');
INSERT INTO Orders VALUES (600, 21112, 9000, '10/JAN/10');

INSERT INTO Orders VALUES (300, 21111, 4580, '10/JAN/10');
INSERT INTO Orders VALUES (400, 21111, 7895, '10/JAN/10');
INSERT INTO Orders VALUES (800, 21111, 7475, '10/JAN/10');

INSERT INTO Orders VALUES (500, 21113, 3505, '10/JAN/10');
INSERT INTO Orders VALUES (1000, 21113, 7850, '10/JAN/10');

--

INSERT INTO Orders VALUES (100, 31112, 3000, '10/JAN/10');
INSERT INTO Orders VALUES (200, 31112, 4500, '10/JAN/10');
INSERT INTO Orders VALUES (600, 31112, 9000, '10/JAN/10');

INSERT INTO Orders VALUES (300, 31111, 4580, '10/JAN/10');
INSERT INTO Orders VALUES (400, 31111, 7895, '10/JAN/10');
INSERT INTO Orders VALUES (800, 31111, 7475, '10/JAN/10');

INSERT INTO Orders VALUES (500, 31113, 3505, '10/JAN/10');
INSERT INTO Orders VALUES (1000, 31113, 7850, '10/JAN/10');

--

INSERT INTO Orders VALUES (100, 41112, 3000, '10/JAN/10');
INSERT INTO Orders VALUES (200, 41112, 4500, '10/JAN/10');
INSERT INTO Orders VALUES (600, 41112, 9000, '10/JAN/10');

INSERT INTO Orders VALUES (300, 41111, 4580, '10/JAN/10');
INSERT INTO Orders VALUES (400, 41111, 7895, '10/JAN/10');
INSERT INTO Orders VALUES (800, 41111, 7475, '10/JAN/10');

INSERT INTO Orders VALUES (500, 41113, 3505, '10/JAN/10');
INSERT INTO Orders VALUES (1000, 41113, 7850, '10/JAN/10');

--

INSERT INTO Orders VALUES (100, 51112, 3000, '10/JAN/10');
INSERT INTO Orders VALUES (200, 51112, 4500, '10/JAN/10');
INSERT INTO Orders VALUES (600, 51112, 9000, '10/JAN/10');

INSERT INTO Orders VALUES (300, 51111, 4580, '10/JAN/10');
INSERT INTO Orders VALUES (400, 51111, 7895, '10/JAN/10');
INSERT INTO Orders VALUES (800, 51111, 7475, '10/JAN/10');

INSERT INTO Orders VALUES (500, 51113, 3505, '10/JAN/10');
INSERT INTO Orders VALUES (1000, 51113, 7850, '10/JAN/10');

INSERT INTO Users VALUES ('admin', 'admin', 'A', '');
INSERT INTO Users VALUES ('test1', 'test1', 'D', 'School A');
INSERT INTO Users VALUES ('test2', 'test2', 'S', 'School B');
INSERT INTO Users VALUES ('test3', 'test3', 'E', 'School C');
INSERT INTO Users VALUES ('test4', 'test4', 'C', 'School D');

COMMIT;